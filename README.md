# The Island of TeX SyncTeX parser library

## What is SyncTeX and why an own parser?

SyncTeX is a synchronisation technology supported by all major engines. It
maps the input (input files, lines, columns) to boxes and point positions
in the output. Therefore it can be used for forward and backward
synchronisation, i.e. finding the appropriate point in the output PDF for
the current input line or finding the respective input line for the current
point in the output.

The tool itself is an application written in C used for parsing the synctex
files and returning the correct data (either line number or position). This
command-line tool only requires the program which is available in TeX Live.
It may even be redistributed for the sole purpose of doing the parsing for
another application.

The [manpage](https://github.com/jlaurens/synctex/blob/master/man5/synctex.5)
of SyncTeX explicitly suggests to use the command-line application for every
parsing of synctex files:

> The structure of this file should not be considered public, in the sense
> that no one should need to parse its contents, except the synctex command
> line utility, and the synctex_parser library.  Unless it is absolutely not
> avoidable, access to the contents of the synctex file should only be made
> through requests made to the synctex command line utility.

So according to this page we need to have a good reason to implement an own
parser and this reason for us is to avoid system calls and JNI (Java native
interface) as far as possible. Therefore we decided in favour of providing
an own scanner.

## Some background on the creation

While collecting ideas for some other tools, we came to the conclusion that
there is no comprehensible structure definition of a SyncTeX file. So we
started to implement a parser, partially as an exercise to get to know the
file format and partially to provide some structured representation to the
community.

It is important to note that we have not looked at the official SyncTeX parser
while implementing this component and we do not intend to. We solely took the
manpage and started to look at real world examples. Hence, this is neither
official nor guaranteed to parse all SyncTeX files. That said, you are very
welcome to open issues and merge requests contributing test cases where this
fails.