// SPDX-License-Identifier: BSD-3-Clause

import org.gradle.api.tasks.testing.logging.TestLogEvent
import org.jetbrains.kotlin.gradle.plugin.KotlinPluginWrapper
import org.jetbrains.kotlin.gradle.tasks.KotlinCompile

repositories {
  jcenter()
}

plugins {
  kotlin("jvm") version "1.4.0"
  `java-library`
  `maven-publish`
  id("com.github.ben-manes.versions") version "0.29.0"  // Apache 2.0
  id("com.diffplug.spotless-changelog") version "2.0.0" // Apache 2.0
  id("org.jetbrains.dokka") version "1.4.0-rc"          // Apache 2.0
  id("io.gitlab.arturbosch.detekt") version "1.11.1"    // Apache 2.0
  id("com.diffplug.spotless") version "5.1.1"           // Apache 2.0
}

val kotlinVersion = plugins.getPlugin(KotlinPluginWrapper::class).kotlinPluginVersion
dependencies {
  implementation(kotlin("stdlib", kotlinVersion))                      // Apache 2.0
  implementation("io.github.microutils:kotlin-logging:1.7.9")          // Apache 2.0
  testImplementation("io.kotest:kotest-runner-junit5-jvm:4.2.0.RC2")   // Apache 2.0
  testImplementation("io.kotest:kotest-assertions-core-jvm:4.2.0.RC2") // Apache 2.0
  testRuntimeOnly("org.slf4j:slf4j-simple:2.0.0-alpha1")               // MIT
}

group = "org.islandoftex"
version = project.spotlessChangelog.versionNext
status = "development"
description = "Synctex parser library"
val modulename = "$group.synctexparser"

spotlessChangelog {
  setAppendDashSnapshotUnless_dashPrelease(false)
  tagPrefix("v")
  commitMessage("Release v{{version}}")
  remote("origin")
  branch("master")
}

java {
  sourceCompatibility = JavaVersion.VERSION_1_8
  targetCompatibility = sourceCompatibility
  withSourcesJar()
  withJavadocJar()
}

kotlin {
  explicitApi()
}

sourceSets {
  main {
    java { setSrcDirs(listOf("src/main/kotlin")) }
    resources { setSrcDirs(listOf("src/main/resources")) }
  }
  test {
    java { setSrcDirs(listOf("src/test/kotlin")) }
    resources { setSrcDirs(listOf("src/test/resources")) }
  }
}

tasks {
  dokkaHtml {
    dokkaSourceSets {
      configureEach {
        jdkVersion = 8
        moduleDisplayName = "${project.group}.synctex-parser"
        includeNonPublic = false
        skipDeprecated = false
        reportUndocumented = true
        skipEmptyPackages = true
        platform = "jvm"
        sourceLink {
          path = "./"
          url = "https://gitlab.com/islandoftex/libraries/synctex-parser"
          lineSuffix = "#L"
        }
        noStdlibLink = false
        noJdkLink = false
      }
    }
  }
  named<Jar>("javadocJar") {
    from(dokkaJavadoc)
  }


  withType<KotlinCompile>().configureEach {
    kotlinOptions.jvmTarget = "1.8"
  }
  withType<Jar>().configureEach {
    manifest.attributes.apply {
      put("Implementation-Title", project.name)
      put("Implementation-Version", project.version)
      put("Automatic-Module-Name", modulename)
    }
    from(project.projectDir) {
      include("LICENSE.md")
      into("META-INF")
    }
  }
  withType<Test>().configureEach {
    useJUnitPlatform()
    testLogging {
      events(
        TestLogEvent.FAILED, TestLogEvent.PASSED, TestLogEvent.SKIPPED,
        TestLogEvent.STANDARD_ERROR, TestLogEvent.STANDARD_OUT
      )
    }
  }
}

publishing {
  publications {
    create<MavenPublication>("GitLab") {
      groupId = project.group.toString()
      artifactId = "synctex-parser"
      version = project.version.toString() +
          if (project.status.toString() == "development") "-SNAPSHOT" else ""

      pom {
        name.set("Island of TeX SyncTeX parser library")
        description.set(
          "A SyncTeX parser written in Kotlin. This is " +
              "not related to the official SyncTeX project " +
              "but rather a reimplementation of the " +
              "structure of a SyncTeX file based on the " +
              "manpage and real-world output."
        )
        inceptionYear.set("2019")
        url.set("https://gitlab.com/islandoftex/libraries/synctex-parser")
        organization {
          name.set("Island of TeX")
          url.set("https://gitlab.com/islandoftex")
        }
        licenses {
          license {
            name.set("BSD 3-clause \"New\" or \"Revised\" License")
            url.set("https://gitlab.com/islandoftex/libraries/synctex-parser/blob/master/LICENSE.md")
          }
        }
        scm {
          connection.set("scm:git:git://gitlab.com/islandoftex/libraries/synctex-parser.git")
          developerConnection.set("scm:git:ssh://git@gitlab.com:islandoftex/libraries/synctex-parser.git")
          url.set("https://gitlab.com/islandoftex/libraries/synctex-parser")
        }
        ciManagement {
          system.set("GitLab")
          url.set("https://gitlab.com/islandoftex/libraries/synctex-parser/pipelines")
        }
        issueManagement {
          system.set("GitLab")
          url.set("https://gitlab.com/islandoftex/libraries/synctex-parser/issues")
        }
      }

      from(components["java"])
      artifacts {
        archives(tasks["sourcesJar"])
        archives(tasks["javadocJar"])
      }
    }
  }

  repositories {
    maven {
      url = uri("https://gitlab.com/api/v4/projects/14173893/packages/maven")
      credentials(HttpHeaderCredentials::class) {
        if (project.hasProperty("jobToken")) {
          name = "Job-Token"
          value = project.property("jobToken").toString()
        }
      }
      authentication {
        create<HttpHeaderAuthentication>("header")
      }
    }
  }
}

detekt {
  buildUponDefaultConfig = true
  config = files("detekt.yml")
}

spotless {
  kotlinGradle {
    trimTrailingWhitespace()
    endWithNewline()
  }
  kotlin {
    ktlint()
    licenseHeader("// SPDX-License-Identifier: BSD-3-Clause")
    trimTrailingWhitespace()
    endWithNewline()
  }
}
