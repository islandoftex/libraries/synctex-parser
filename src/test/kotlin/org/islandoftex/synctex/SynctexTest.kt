// SPDX-License-Identifier: BSD-3-Clause
package org.islandoftex.synctex

import io.kotest.assertions.throwables.shouldThrow
import io.kotest.core.spec.style.ShouldSpec
import io.kotest.matchers.shouldBe
import io.kotest.matchers.string.shouldContain

/**
 * This class tests the behaviour of the synctex parser.
 *
 * @since 0.1.0
 */
class SynctexTest : ShouldSpec({
    val basePath = "/${SynctexParser.javaClass.`package`.name.replace(".", "/")}"

    context("test file 01") {
        val synctexFile = SynctexParser.parse(
            SynctexTest::class.java
                .getResource("$basePath/test01.synctex")
                .readText()
        )

        should("have correct preamble") {
            with(synctexFile.preamble) {
                synctexVersion.toFloat() shouldBe 1f
                output shouldBe "pdf"
                magnification shouldBe 1000
                unit shouldBe 1
                xoffset shouldBe 0
                yoffset shouldBe 0
            }
        }

        should("have correct input list") {
            with(synctexFile.preamble.inputLines) {
                get(0) shouldBe SynctexInput(1, "/./testfile.tex")
                get(1) shouldBe SynctexInput(
                    2,
                    "/usr/local/texlive/2018/texmf-dist/tex/latex/base/article.cls"
                )
                get(2) shouldBe SynctexInput(
                    3,
                    "/usr/local/texlive/2018/texmf-dist/tex/latex/base/size10.clo"
                )
                get(3) shouldBe SynctexInput(
                    4,
                    "/usr/local/texlive/2018/texmf-dist/tex/latex/l3kernel/expl3.sty"
                )
                get(4) shouldBe SynctexInput(
                    5,
                    "/usr/local/texlive/2018/texmf-dist/tex/latex/l3kernel/expl3-code.tex"
                )
                get(5) shouldBe SynctexInput(
                    6,
                    "/usr/local/texlive/2018/texmf-dist/tex/latex/l3kernel/l3pdfmode.def"
                )
                get(6) shouldBe SynctexInput(7, "/./testfile.aux")
            }
        }

        should("read correct content") {
            with(synctexFile.content.sheets.entries.elementAt(0)) {
                key shouldBe SynctexInput(
                    SynctexInput.UNIDENTIFIED_INPUT,
                    ""
                )
                value[0].byteRange shouldBe IntRange(564, 626)
            }
        }

        should("have correct postamble") {
            synctexFile.postamble.count shouldBe 28
        }

        should("have correct post scriptum") {
            with(synctexFile.postscriptum) {
                magnification shouldBe Int.MIN_VALUE
                xoffset shouldBe Int.MIN_VALUE
                yoffset shouldBe Int.MIN_VALUE
            }
        }
    }

    context("test files 02–03") {
        should("throw missing postamble exception") {
            val ex = shouldThrow<SynctexParser.SynctexParseException> {
                SynctexParser.parse(
                    SynctexTest::class.java
                        .getResource("$basePath/test02.synctex").readText()
                )
            }
            ex.message.shouldContain("Count")
        }
        should("throw missing content exception") {
            val ex = shouldThrow<SynctexParser.SynctexParseException> {
                SynctexParser.parse(
                    SynctexTest::class.java
                        .getResource("$basePath/test03.synctex").readText()
                )
            }
            ex.message.shouldContain("Content")
        }
    }

    context("test file 10") {
        should("throw missing delimiter exception") {
            val ex = shouldThrow<SynctexParser.SynctexParseException> {
                SynctexParser.parse(
                    SynctexTest::class.java
                        .getResource("$basePath/test10.synctex").readText()
                )
            }
            ex.message.shouldContain("box opened with [")
        }
    }
})
