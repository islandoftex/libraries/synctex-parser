module org.islandoftex.synctex.synctexparser {
  requires kotlin.stdlib;

  requires kotlin.logging;

  exports org.islandoftex.synctexparser;
}
