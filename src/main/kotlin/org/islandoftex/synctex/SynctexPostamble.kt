// SPDX-License-Identifier: BSD-3-Clause
package org.islandoftex.synctex

/**
 * This resembles the synctex postamble which closes the file (usually).
 * According to the manpage: "If there is no postamble, it means that the
 * typesetting process did not end correctly."
 *
 * @since 0.1.0
 *
 * @property count This is the number of records present in the synctex file.
 *  @since 0.1.0
 */
public data class SynctexPostamble(val count: Int)
