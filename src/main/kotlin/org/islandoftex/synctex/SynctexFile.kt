// SPDX-License-Identifier: BSD-3-Clause
package org.islandoftex.synctex

/**
 * This class resembles a synctex file with all its four components: preamble,
 * content, postamble and postscriptum.
 *
 * @since 0.1.0
 *
 * @property preamble The preamble of the synctex file.
 *  @since 0.1.0
 *
 * @property content The content of the synctex file.
 *  @since 0.1.0
 *
 * @property postamble The postamble of the synctex file.
 *  @since 0.1.0
 *
 * @property postscriptum The postscriptum of the synctex file. May be absent.
 *  @since 0.1.0
 */
public data class SynctexFile(
    val preamble: SynctexPreamble,
    val content: SynctexContent,
    val postamble: SynctexPostamble,
    val postscriptum: SynctexPostScriptum
)
