// SPDX-License-Identifier: BSD-3-Clause
package org.islandoftex.synctex

/**
 * This class resembles a form in SyncTeX. Each form is identified by a tag and
 * contains boxes and records. Forms may be used on top-level and in the box
 * content section.
 *
 * This implements [SynctexNode] to ensure that it may be used at content-level.
 *
 * @since 0.1.0
 *
 * @property tag The identifier of the form. In the file identified by an
 *  opening angled bracket followed by it.
 *  @since 0.1.0
 *
 * @property boxes This list contains all records and boxes associated with the
 *  sheet.
 *  @since 0.1.0
 *
 * @property byteRange This range of integers relates to the two byte offset
 *  records a sheet has (one before the opening and one before the closing
 *  brace).
 *  @since 0.1.0
 */
public data class SynctexForm(
    val tag: Int,
    val boxes: List<SynctexNode>,
    override val byteRange: IntRange
) : SynctexNode, SynctexTopLevelNode
