// SPDX-License-Identifier: BSD-3-Clause
package org.islandoftex.synctex

/**
 * This class resembles a sheet (basically a page) in SyncTeX. Each sheet is
 * identified by a number (tag) and contains boxes and records.
 *
 * @since 0.1.0
 *
 * @property tag The identifier of the sheet. In the file identified by an
 *  opening curly brace followed by it.
 *  @since 0.1.0
 *
 * @property boxes This list contains all records and boxes associated with the
 *  sheet.
 *  @since 0.1.0
 *
 * @property byteRange This range of integers relates to the two byte offset
 *  records a sheet has (one before the opening and one before the closing
 *  brace).
 *  @since 0.1.0
 */
public data class SynctexSheet(
    val tag: Int,
    val boxes: List<SynctexNode>,
    override val byteRange: IntRange
) : SynctexTopLevelNode
