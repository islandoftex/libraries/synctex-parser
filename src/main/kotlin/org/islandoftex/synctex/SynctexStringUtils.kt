// SPDX-License-Identifier: BSD-3-Clause
package org.islandoftex.synctex

/**
 * Utility functions that extract synctex boxes and records from a line of the
 * synctex file.
 *
 * @since 0.2.0
 */
internal object SynctexStringUtils {
    /**
     * Parse a line for a synctex form reference record.
     *
     * @since 0.2.0
     * @param source The line to parse.
     * @return The form record.
     * @throws SynctexParser.SynctexParseException If the type of the line does
     *   not correspond to the record type.
     */
    @JvmStatic
    fun handleFormRefRecord(source: String): SynctexFormRefRecord {
        if (!source.contains("f"))
            throw SynctexParser.SynctexParseException("Not a form reference record.")
        val (tag, point) = source.split(":", limit = 2)
        val tagResult = tag.removePrefix("f").trim().toInt()
        val pointList = point.split(",").map { it.toInt() }
        val pointResult = Pair(pointList[0], pointList[1])
        return SynctexFormRefRecord(tag = tagResult, point = pointResult)
    }

    /**
     * Parse a line for a synctex kern record.
     *
     * @since 0.2.0
     * @param source The line to parse.
     * @return The kern record.
     * @throws SynctexParser.SynctexParseException If the type of the line does
     *   not correspond to the record type.
     */
    @JvmStatic
    fun handleKernRecord(source: String): SynctexKernRecord {
        if (!source.contains("k"))
            throw SynctexParser.SynctexParseException("Not a kern record.")
        val (link, point, width) = source.split(":", limit = 3)
        val linkList = link.removePrefix("k").split(",").map { it.toInt() }
        val linkResult = Triple(linkList[0], linkList[1], linkList.getOrNull(2))
        val pointList = point.split(",").map { it.toInt() }
        val pointResult = Pair(pointList[0], pointList[1])
        return SynctexKernRecord(link = linkResult, point = pointResult, width = width.toInt())
    }

    /**
     * Parse a line for a synctex current record.
     *
     * @since 0.2.0
     * @param source The line to parse.
     * @return The current record.
     * @throws SynctexParser.SynctexParseException If the type of the line does
     *   not correspond to the record type.
     */
    @JvmStatic
    fun handleCurrentRecord(source: String): SynctexCurrentRecord {
        if (!source.contains("x"))
            throw SynctexParser.SynctexParseException("Not a current record.")
        val (linkResult, pointResult) = handleTwoPartRecord(source)
        return SynctexCurrentRecord(link = linkResult, point = pointResult)
    }

    /**
     * Parse a line for a synctex glue record.
     *
     * @since 0.2.0
     * @param source The line to parse.
     * @return The glue record.
     * @throws SynctexParser.SynctexParseException If the type of the line does
     *   not correspond to the record type.
     */
    @JvmStatic
    fun handleGlueRecord(source: String): SynctexGlueRecord {
        if (!source.contains("g"))
            throw SynctexParser.SynctexParseException("Not a glue record.")
        val (linkResult, pointResult) = handleTwoPartRecord(source)
        return SynctexGlueRecord(link = linkResult, point = pointResult)
    }

    /**
     * Parse a line for a synctex math record.
     *
     * @since 0.2.0
     * @param source The line to parse.
     * @return The math record.
     * @throws SynctexParser.SynctexParseException If the type of the line does
     *   not correspond to the record type.
     */
    @JvmStatic
    fun handleMathRecord(source: String): SynctexMathRecord {
        if (!source.contains("$"))
            throw SynctexParser.SynctexParseException("Not a math record.")
        val (linkResult, pointResult) = handleTwoPartRecord(source)
        return SynctexMathRecord(link = linkResult, point = pointResult)
    }

    /**
     * Parse a line for a synctex void vertical box.
     *
     * @since 0.2.0
     * @param source The line to parse.
     * @return The void vbox.
     * @throws SynctexParser.SynctexParseException If the type of the line does
     *   not correspond to the record type.
     */
    @JvmStatic
    fun handleVoidVBox(source: String): SynctexVoidVBox {
        if (!source.contains("v"))
            throw SynctexParser.SynctexParseException("Not a void vertical box.")
        val (link, point, size) = handleVoidBox(source)
        return SynctexVoidVBox(link = link, point = point, size = size)
    }

    /**
     * Parse a line for a synctex void horizontal box.
     *
     * @since 0.2.0
     * @param source The line to parse.
     * @return The void hbox.
     * @throws SynctexParser.SynctexParseException If the type of the line does
     *   not correspond to the record type.
     */
    @JvmStatic
    fun handleVoidHBox(source: String): SynctexVoidHBox {
        if (!source.contains("h"))
            throw SynctexParser.SynctexParseException("Not a void horizontal box.")
        val (link, point, size) = handleVoidBox(source)
        return SynctexVoidHBox(link = link, point = point, size = size)
    }

    /**
     * Handle splitting and parsing of a line for records.
     *
     * @since 0.2.0
     * @param source The source line.
     * @return A pair consisting of link and point.
     */
    private fun handleTwoPartRecord(source: String): Pair<Triple<Int, Int, Int?>, Pair<Int, Int>> {
        val (link, point) = source.split(":", limit = 2)
        val linkList = link.removePrefix("$")
            .removePrefix("x")
            .removePrefix("g")
            .split(",").map { it.toInt() }
        val linkResult = Triple(linkList[0], linkList[1], linkList.getOrNull(2))
        val pointList = point.split(",").map { it.toInt() }
        val pointResult = Pair(pointList[0], pointList[1])
        return linkResult to pointResult
    }

    /**
     * Handle splitting and parsing of a line for void boxes.
     *
     * @since 0.2.0
     * @param source The source line.
     * @return A triple consisting of link, point and size.
     */
    private fun handleVoidBox(source: String): Triple<Triple<Int, Int, Int?>, Pair<Int, Int>, Triple<Int, Int, Int>> {
        val (link, point, size) = source.split(":")
        val linkList = link
            .removePrefix("h")
            .removePrefix("v")
            .split(",")
            .map { it.toInt() }
        val linkResult = Triple(linkList[0], linkList[1], linkList.getOrNull(2))
        val pointList = point.split(",").map { it.toInt() }
        val pointResult = Pair(pointList[0], pointList[1])
        val sizeList = size.split(",").map { it.toInt() }
        val sizeResult = Triple(sizeList[0], sizeList[1], sizeList[2])
        return Triple(linkResult, pointResult, sizeResult)
    }
}
