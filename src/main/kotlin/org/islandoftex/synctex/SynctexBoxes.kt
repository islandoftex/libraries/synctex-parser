// SPDX-License-Identifier: BSD-3-Clause
package org.islandoftex.synctex

/**
 * This interface represents a basic element (read not a line, but a semantic
 * unit) in SyncTeX.
 *
 * @since 0.1.0
 */
public interface SynctexNode

/**
 * This interface represents a basic top level element (i.e. sheet or form) in
 * SyncTeX.
 *
 * @since 0.1.0
 *
 * @property byteRange This range of integers relates to the two byte offset
 *  records a top level node has (one before the opening and one before the
 *  closing delimiter).
 *  @since 0.1.0
 */
public interface SynctexTopLevelNode {
    public val byteRange: IntRange
}

/**
 * This interface represents a box in TeX/SyncTeX. It is implemented by ordinary
 * boxes as well as void boxes.
 *
 * Basically this design is incorrect. This interface more or less is a union
 * of the concepts of boxes and records in SyncTeX. Therefore implementing
 * classes will make assumptions which might be incorrect.
 *
 * @since 0.1.0
 *
 * @property link This triple represents the internal link form of SyncTeX
 *  consisting of a tag (form/page tag), the line in the source file and an
 *  optional specification of the column. If the column is omitted the third
 *  element of the triple is `null`.
 *  @since 0.1.0
 *
 * @property point A SyncTeX point consists of two integers. That is the full
 *  point. The compressed point consisting of an integer and `=` will be
 *  transformed into a full point.
 *  @since 0.1.0
 *
 * @property size A SyncTeX size is a triple of width, height and depth, each
 *  of them as integers.
 *  @since 0.1.0
 */
public interface SynctexBox : SynctexNode {
    public val link: Triple<Int, Int, Int?>
    public val point: Pair<Int, Int>
    public val size: Triple<Int, Int, Int>
}

/**
 * This class represents a horizontal mode box in synctex.
 *
 * @since 0.1.0
 *
 * @property children The horizontal box may have arbitrary boxes as children.
 *  @since 0.1.0
 */
public data class SynctexHBox(
    override val link: Triple<Int, Int, Int?>,
    override val point: Pair<Int, Int>,
    override val size: Triple<Int, Int, Int>,
    val children: List<SynctexNode>
) : SynctexBox

/**
 * This class represents a vertical mode box in synctex.
 *
 * @since 0.1.0
 *
 * @property children The vertical box may have arbitrary boxes as children.
 *  @since 0.1.0
 */
public data class SynctexVBox(
    override val link: Triple<Int, Int, Int?>,
    override val point: Pair<Int, Int>,
    override val size: Triple<Int, Int, Int>,
    val children: List<SynctexNode>
) : SynctexBox

/**
 * This class represents a void (zero-size) horizontal mode box in synctex.
 *
 * @since 0.1.0
 */
public data class SynctexVoidHBox(
    override val link: Triple<Int, Int, Int?>,
    override val point: Pair<Int, Int>,
    override val size: Triple<Int, Int, Int>
) : SynctexBox

/**
 * This class represents a void (zero-size) vertical mode box in synctex.
 *
 * @since 0.1.0
 */
public data class SynctexVoidVBox(
    override val link: Triple<Int, Int, Int?>,
    override val point: Pair<Int, Int>,
    override val size: Triple<Int, Int, Int>
) : SynctexBox

/**
 * This class represents a synctex kern record. It has only a width and no
 * height or depth.
 *
 * @since 0.1.0
 */
public data class SynctexKernRecord(
    override val link: Triple<Int, Int, Int?>,
    override val point: Pair<Int, Int>,
    private val width: Int
) : SynctexBox {
    override val size: Triple<Int, Int, Int> = Triple(width, 0, 0)
}

/**
 * This is an abstraction of a record in SyncTeX, i.e. a box without size.
 *
 * @since 0.2.0
 */
public abstract class SynctexTwoPartRecord : SynctexBox {
    override val size: Triple<Int, Int, Int> = Triple(0, 0, 0)
}

/**
 * This class represents the current synctex record. It is a zero-sized
 * positional reference.
 *
 * @since 0.1.0
 */
public data class SynctexCurrentRecord(
    override val link: Triple<Int, Int, Int?>,
    override val point: Pair<Int, Int>
) : SynctexTwoPartRecord()

/**
 * This class represents a synctex glue record. It is a zero-sized positional
 * reference.
 *
 * @since 0.1.0
 */
public data class SynctexGlueRecord(
    override val link: Triple<Int, Int, Int?>,
    override val point: Pair<Int, Int>
) : SynctexTwoPartRecord()

/**
 * This class represents a synctex math record. It is a zero-sized positional
 * reference.
 *
 * @since 0.1.0
 */
public data class SynctexMathRecord(
    override val link: Triple<Int, Int, Int?>,
    override val point: Pair<Int, Int>
) : SynctexTwoPartRecord()

/**
 * This class represents a synctex form reference record. It is a zero-sized
 * positional reference which uses only the `tag` property of the [link], i.e.
 * has no line or column reference.
 *
 * @since 0.1.0
 */
public data class SynctexFormRefRecord(
    private val tag: Int,
    override val point: Pair<Int, Int>
) : SynctexBox {
    override val link: Triple<Int, Int, Int?> = Triple(tag, 0, null)
    override val size: Triple<Int, Int, Int> = Triple(0, 0, 0)
}
