// SPDX-License-Identifier: BSD-3-Clause
package org.islandoftex.synctex

/**
 * This class resembles a synctex input directive.
 *
 * @since 0.1.0
 *
 * @property tag The identifier of the input directive. If the input can't be
 *  identified [UNIDENTIFIED_INPUT] will be used.
 *  @since 0.1.0
 *
 * @property file The file name as string.
 *  @since 0.1.0
 */
public data class SynctexInput(
    val tag: Int,
    val file: String
) {
    public companion object {
        /**
         * @property UNIDENTIFIED_INPUT Used if the input directive can't be
         *  identified or the tag is unknown.
         *  @since 0.1.0
         */
        public const val UNIDENTIFIED_INPUT: Int = -1
    }
}
