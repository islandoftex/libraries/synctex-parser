// SPDX-License-Identifier: BSD-3-Clause
package org.islandoftex.synctex

/**
 * This resembles the synctex postscriptum. It contains values by third parties
 * which may override the settings in the preamble. This may be empty, so the
 * default values are relevant.
 *
 * @since 0.1.0
 *
 * @property magnification The magnification value. There's something unclear
 *  in the manpage: it says this sets additional magnification while overriding
 *  the value in the preamble. It defaults to [DEFAULT_VALUE] when not existent.
 *  @since 0.1.0
 *
 * @property xoffset The x offset value as a dimension in the file which differs
 *  from the way the preamble is encoded, hence we store it as scaled points
 *  as the preamble does. There's something unclear in the manpage: it says this
 *  sets additional x offset while overriding the value in the preamble. It
 *  defaults to [DEFAULT_VALUE] when not existent.
 *  @since 0.1.0
 *
 * @property yoffset The y offset value as a dimension in the file which differs
 *  from the way the preamble is encoded, hence we store it as scaled points
 *  as the preamble does. There's something unclear in the manpage: it says this
 *  sets additional y offset while overriding the value in the preamble. It
 *  defaults to [DEFAULT_VALUE] when not existent.
 *  @since 0.1.0
 */
public data class SynctexPostScriptum(
    val magnification: Int = DEFAULT_VALUE,
    val xoffset: Int = DEFAULT_VALUE,
    val yoffset: Int = DEFAULT_VALUE
) {
    public companion object {
        /**
         * @property DEFAULT_VALUE This value describes the default value used
         *  when the post scriptum is uninitialized. It's basically a replacement
         *  for nullability.
         *  @since 0.1.0
         */
        public const val DEFAULT_VALUE: Int = Int.MIN_VALUE
    }
}
