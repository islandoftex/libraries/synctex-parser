// SPDX-License-Identifier: BSD-3-Clause
package org.islandoftex.synctex

import mu.KotlinLogging

/**
 * This is a synctex parser which reads the synctex file (text, not gzipped)
 * and returns a hierarchical object representation as instance of [SynctexFile].
 *
 * ## What is SyncTeX and why an own parser?
 *
 * SyncTeX is a synchronisation technology supported by all major engines. It
 * maps the input (input files, lines, columns) to boxes and point positions
 * in the output. Therefore it can be used for forward and backward
 * synchronisation, i.e. finding the appropriate point in the output PDF for
 * the current input line or finding the respective input line for the current
 * point in the output.
 *
 * The tool itself is an application written in C used for parsing the synctex
 * files and returning the correct data (either line number or position). This
 * command-line tool only requires the program which is available in TeX Live.
 * It may even be redistributed for the sole purpose of doing the parsing for
 * another application.
 *
 * The [manpage](https://github.com/jlaurens/synctex/blob/master/man5/synctex.5)
 * of SyncTeX explicitly suggests to use the command-line application for every
 * parsing of synctex files:
 *
 * > The structure of this file should not be considered public, in the sense
 * > that no one should need to parse its contents, except the synctex command
 * > line utility, and the synctex_parser library.  Unless it is absolutely not
 * > avoidable, access to the contents of the synctex file should only be made
 * > through requests made to the synctex command line utility.
 *
 * So according to this page we need to have a good reason to implement an own
 * parser and this reason for us is to avoid system calls and JNI (Java native
 * interface) as far as possible. Therefore we decided in favour of providing
 * an own scanner.
 *
 * ## SyncTeX syntactical analysis
 *
 * Although the cited manpage contains an EBNF representation of the SyncTeX
 * file we found that this does not correspond to the actual output of the
 * implementation (in TeX engines). Therefore we compiled the following grammar
 * for SyncTeX files based on the analysis of SyncTeX files generated with
 * TeX Live 2018.
 *
 * This grammar will use ABNF to represent the syntactical elements. Please
 * note that we use a code block to present the BNF due to markdown's
 * limitations.
 *
 * ### The file as a whole
 *
 *     SyncTeX = Preamble Content Postamble PostScriptum
 *
 * The SyncTeX file is divided into four main parts which start with a line
 * identifying the current part (except preamble). Each of the parts has to
 * present for the file to be a valid synctex file, althought that may include
 * an empty section (i.e. for the PostScriptum).
 *
 * One of the most important components is the postamble, because it allows to
 * decide whether typesetting succeeded. From the manpage:
 *
 * > The postamble closes the file. If there is no postamble, it means that the
 * > typesetting process did not end correctly.
 *
 * Further information from the manpage:
 *
 * > All the numbers are integers encoded using the decimal representation with
 * > "C" locale.
 *
 * ### The preamble
 *
 *     Preamble           = "SyncTeX Version:" VersionNumber EOL
 *                          *InputLine
 *                          "Output:" 1*VCHAR EOL
 *                          "Magnification:" TeXMagnification EOL
 *                          "Unit:" UnitSP EOL
 *                          "X Offset:" HorizontalOffsetSP EOL
 *                          "Y Offset:" VerticalOffsetSP EOL
 *     VersionNumber      = 1*DIGIT ; may it be float?
 *     UnitSP             = 1*DIGIT ; in small points
 *     HorizontalOffsetSP = 1*DIGIT ; in small points
 *     VerticalOffsetSP   = 1*DIGIT ; in small points
 *     InputLine          = "Input:" Tag ":" FileName EOL
 *     Tag                = 1*DIGIT
 *     FileName           = 1*VCHAR
 *     EOL                = CR / LF / CRLF
 *
 * ### The content
 *
 *     Content          = "Content:" EOL
 *                        1*(
 *                          *(Form / InputLine)
 *                          Sheet
 *                        )
 *                        *(Form / InputLine)
 *     ByteOffsetRecord = "!" ByteOffset EOL
 *     ByteOffset       = 1*DIGIT
 *     Sheet            = ByteOffsetRecord
 *                        "{" Tag EOL
 *                        (VboxSection / HboxSection)
 *                        ByteOffsetRecord
 *                        "}" Tag EOL ; same as first Tag
 *     Form             = ByteOffsetRecord
 *                        "<" Tag EOL
 *                        (VboxSection / HboxSection)
 *                        ByteOffsetRecord
 *                        ">" EOL
 *
 * From the manpage:
 *
 * > Forms are available with pdfTeX. […]  The `BoxContent` describes what is
 * > inside a box. It is either a vertical or horizontal box, with some records
 * > related to glue, kern or math nodes.
 *
 *     BoxContent  = VboxSection / HboxSection / VoidVboxRecord /
 *                   VoidHboxRecord / CurrentRecord / GlueRecord / KernRecord /
 *                   MathRecord / FormRefRecord / Form
 *     VboxSection = "[" Link ":" Point ":" Size EOL
 *                   *BoxContent
 *                   "]" EOL
 *     HboxSection = "(" Link ":" Point ":" Size EOL
 *                   *BoxContent
 *                   ")" EOL
 *
 *     VoidVboxRecord  = "v" Link ":" Point ":" Size EOL
 *     VoidHboxRecord  = "h" Link ":" Point ":" Size EOL
 *     Link            = Tag "," Line ["," Column]
 *     Point           = FullPoint / CompressedPoint
 *     FullPoint       = *DIGIT "," *DIGIT
 *     CompressedPoint = *DIGIT ",="
 *     Line            = *DIGIT
 *     Column          = *DIGIT
 *     Size            = Width "," Height "," Depth
 *     Width           = *DIGIT
 *     Height          = *DIGIT
 *     Depth           = *DIGIT
 *
 *     CurrentRecord = "x" Link ":" Point EOL
 *     KernRecord    = "k" Link ":" Point ":" Width EOL
 *     GlueRecord    = "g" Link ":" Point EOL
 *     MathRecord    = "$" Link ":" Point EOL
 *     FormRefRecord = "f" Tag ":" Point EOL ; Tag is a tag of a form
 *
 * From the manpage:
 *
 * > The `CurrentRecord` is used to compute the visible size of hbox's. The
 * > byte offset is an implicit anchor to navigate the synctex file from sheet
 * > to sheet. The second coordinate of a compressed point has been replaced
 * > by a "=" character which means that it is the second coordinate of the
 * > last full point available above.
 *
 * ### The Postamble
 *
 *     Postamble       = ByteOffsetRecord
 *                       "Postamble:" EOL
 *                       "Count:" NumberOfRecords EOL
 *     NumberOfRecords = *DIGIT
 *
 * ### The PostScriptum
 *
 * From the manpage:
 *
 * > The post scriptum contains material possibly added by 3rd parties. It
 * > allows to append some transformation (shift and magnify). Typically, one
 * > applies a dvi to pdf filter with offset options and magnification, then he
 * > appends the same options to the synctex file, for example
 * > `synctex update -o foo.pdf -m 0.486 -x 9472573sp -y 13.3dd source.dvi`
 *
 *     PostScriptum = ByteOffsetRecord
 *                   "Post Scriptum:" EOL
 *                   ["Magnification:" TeXMagnification EOL] ; additional magn.
 *                   ["X Offset:" Dimension EOL] ; horizontal offset
 *                   ["Y Offset:" Dimension EOL] ; vertical offset
 *
 * From the manpage:
 *
 * > This second information will override the offset and magnification
 * > previously available in the preamble section.
 *
 * @since 0.1.0
 */
// TODO: definitions Dimension and additional magnification
public object SynctexParser {
    /**
     * @property logger The logger instance for this object
     *  @since 0.1.0
     */
    private val logger = KotlinLogging.logger { }

    /**
     * This class is just a semantic implementation of an exception thrown if the
     * parser encounters synctex syntax related errors.
     *
     * @since 0.1.0
     */
    public class SynctexParseException(message: String) : Exception(message)

    /**
     * This function reads a new-line delimited list from a string splitting each
     * item at the first colon into key and value.
     *
     * @since 0.1.0
     * @param text The text to split into map entries.
     * @return This method returns a non-mutable map of string to string.
     */
    private fun readToMap(text: String): Map<String, String> {
        return text.split("\n")
            .asSequence()
            .filter { it.contains(":") }
            .map { line ->
                val (key, value) = line.split(":", limit = 2)
                key.trim() to value.trim()
            }
            .groupBy { it.first }
            .mapValues {
                it.value.joinToString("\n") { pair -> pair.second }
            }
    }

    /**
     * This function checks for a closing matching delimiter on the same level
     * as the given opening one. The initial state is opened, so please give
     * the list or indices accordingly (should not contain the opening line).
     *
     * @param list The list of lines to check
     * @param open The opening delimiter to count
     * @param close The closing delimiter to count
     * @param startIndex Start index for search (inclusive)
     * @param stopIndex Stop index for search (inclusive)
     *
     * @return The index of the closing character within the list of lines.
     *
     * @throws SynctexParseException If an opened box is never closed.
     *
     * @since 0.1.0
     */
    @Throws(SynctexParseException::class)
    private fun findClosingIndex(
        list: List<String>,
        open: Char,
        close: Char,
        startIndex: Int = 0,
        stopIndex: Int = list.size - 1
    ): Int {
        // if open then > 0
        var openClose = 1
        (startIndex..stopIndex).forEach { index ->
            openClose += list[index].count { it == open } -
                    list[index].count { it == close }
            if (openClose == 0)
                return index
        }
        throw SynctexParseException(
            "Invalid file format: box opened with $open " +
                    "is never closed by $close"
        )
    }

    /**
     * This function collects all boxes which are in the synctex string.
     *
     * @since 0.1.0
     * @param text The string to scan for synctex nodes.
     * @return This returns a list of synctex nodes. It may be empty.
     * @throws SynctexParseException If the file format is invalid.
     */
    @Throws(SynctexParseException::class)
    private fun findBoxesInString(text: String): List<SynctexNode> {
        val retList = mutableListOf<SynctexNode>()
        val contentLines = text.split("\n")
        var lineCounter = 0
        while (lineCounter < contentLines.size - 1) {
            val it = contentLines[lineCounter]
            when {
                it.startsWith("!") -> {
                    // <?form tag?\n <vbox section>|<hbox section>\n!BOR\n> -> form
                    // TODO: technically this should check if only vboxes or hboxes are
                    // on top level, shouldn't it?

                    // check that the byte offset record really belongs to the form
                    if (contentLines.size > lineCounter + 1 &&
                        contentLines[lineCounter + 1].startsWith("<")
                    ) {
                        val nextit = contentLines[lineCounter + 1]
                        try {
                            val byteOffsetStart = it.removePrefix("!").trim().toInt()
                            val tag = nextit.removePrefix("<").trim().toInt()
                            val closingAt = findClosingIndex(
                                contentLines, '<', '>',
                                lineCounter + 2
                            )
                            val byteOffsetEnd = contentLines[closingAt - 1]
                                .removePrefix("!").trim().toInt()
                            val children = findBoxesInString(
                                contentLines
                                    .subList(lineCounter + 2, closingAt - 1)
                                    .joinToString("\n")
                            )
                            retList.add(
                                SynctexForm(
                                    tag = tag, boxes = children,
                                    byteRange = IntRange(byteOffsetStart, byteOffsetEnd)
                                )
                            )
                            // set next line manually and prevent increment
                            lineCounter = closingAt + 1
                        } catch (ex: Exception) {
                            // ex is IndexOutOfBoundsException or SynctexParseException
                            throw SynctexParseException(
                                "Invalid file format: Malformed " +
                                        "synctex form detected which has " +
                                        "thrown ${ex.message}"
                            )
                        }
                    } else {
                        logger.warn {
                            "Invalid file format: Orphaned byte offset record detected"
                        }
                        lineCounter++
                    }
                }
                it.startsWith("[") -> {
                    // [link:point:size\n box\n] -> vbox
                    val (link, point, size) = it.split(":")
                    val linkList = link.removePrefix("[")
                        .split(",")
                        .map { it.toInt() }
                    val linkResult = Triple(linkList[0], linkList[1], linkList.getOrNull(2))
                    val pointList = point.split(",").map { it.toInt() }
                    val pointResult = Pair(pointList[0], pointList[1])
                    val sizeList = size.split(",").map { it.toInt() }
                    val sizeResult = Triple(sizeList[0], sizeList[1], sizeList[2])
                    val closingAt = findClosingIndex(
                        contentLines, '[', ']',
                        lineCounter + 1
                    )
                    val children = findBoxesInString(
                        contentLines
                            .subList(lineCounter + 1, closingAt)
                            .joinToString("\n")
                    )
                    retList.add(
                        SynctexVBox(
                            link = linkResult, point = pointResult,
                            size = sizeResult, children = children
                        )
                    )
                    // set next line manually and prevent increment
                    lineCounter = closingAt + 1
                }
                it.startsWith("(") -> {
                    // (link:point:size\n box\n) -> hbox
                    val (link, point, size) = it.split(":")
                    val linkList = link.removePrefix("(")
                        .split(",").map { it.toInt() }
                    val linkResult = Triple(linkList[0], linkList[1], linkList.getOrNull(2))
                    val pointList = point.split(",").map { it.toInt() }
                    val pointResult = Pair(pointList[0], pointList[1])
                    val sizeList = size.split(",").map { it.toInt() }
                    val sizeResult = Triple(sizeList[0], sizeList[1], sizeList[2])
                    val closingAt = findClosingIndex(
                        contentLines, '(', ')',
                        lineCounter + 1
                    )
                    val children = findBoxesInString(
                        contentLines
                            .subList(lineCounter + 1, closingAt)
                            .joinToString("\n")
                    )
                    retList.add(
                        SynctexHBox(
                            link = linkResult, point = pointResult,
                            size = sizeResult, children = children
                        )
                    )
                    // set next line manually and prevent increment
                    lineCounter = closingAt + 1
                }
                it.startsWith("v") -> {
                    // v?link?:?point?:?size? -> void vbox
                    retList.add(SynctexStringUtils.handleVoidVBox(it))
                    lineCounter++
                }
                it.startsWith("h") -> {
                    // h?link?:?point?:?size? -> void hbox
                    retList.add(SynctexStringUtils.handleVoidHBox(it))
                    lineCounter++
                }
                it.startsWith("x") -> {
                    // x?link?:?point? -> current record
                    retList.add(SynctexStringUtils.handleCurrentRecord(it))
                    lineCounter++
                }
                it.startsWith("g") -> {
                    // g?link?:?point? -> glue record
                    retList.add(SynctexStringUtils.handleGlueRecord(it))
                    lineCounter++
                }
                it.startsWith("$") -> {
                    // $?link?:?point? -> math record
                    retList.add(SynctexStringUtils.handleMathRecord(it))
                    lineCounter++
                }
                it.startsWith("k") -> {
                    // k?link?:?point?:?Width? -> kern record
                    retList.add(SynctexStringUtils.handleKernRecord(it))
                    lineCounter++
                }
                it.startsWith("f") -> {
                    // f?form tag?:?point? -> form ref record
                    retList.add(SynctexStringUtils.handleFormRefRecord(it))
                    lineCounter++
                }
                else -> // this should not happen
                    logger.warn { "A strange line occurred (boxing): $it" }
            }
        }
        return retList
    }

    /**
     * This function parses the string representation of the Content section to
     * a [SynctexContent] object.
     *
     * @since 0.1.1
     * @param content The string representation of the content.
     * @return The parsed content.
     * @throws SynctexParseException If a top level node is not properly ended
     *   by a byte offset record an exception will be thrown.
     */
    @Throws(SynctexParseException::class)
    private fun getSyncTeXContent(content: String): SynctexContent {
        val contentMap = mutableMapOf<SynctexInput, List<SynctexTopLevelNode>>()
        val contentLines = content.split("\n").filter { it.isNotBlank() }
        var currentInput = SynctexInput(SynctexInput.UNIDENTIFIED_INPUT, "")
        val currentInputList = mutableListOf<SynctexTopLevelNode>()
        var lineCounter = 0
        loop@ while (lineCounter < contentLines.size) {
            val it = contentLines[lineCounter]
            val nextit = contentLines.getOrElse(lineCounter + 1) { "" }
            when {
                it.startsWith("!") -> // we ought to have a toplevelnode next
                    if (nextit.startsWith("<") || nextit.startsWith("{")) {
                        val byteOffsetStart = it.removePrefix("!").trim().toInt()
                        val tag = nextit.substring(1).trim().toInt()
                        val closingAt = if (nextit.startsWith("<")) {
                            // there's a top-level synctex form
                            findClosingIndex(contentLines, '<', '>', lineCounter + 2)
                        } else {
                            // there's a top-level synctex sheet
                            findClosingIndex(contentLines, '{', '}', lineCounter + 2)
                        }
                        val byteOffsetEndLine = contentLines[closingAt - 1]
                        val byteOffsetEnd = if (byteOffsetEndLine.startsWith("!"))
                            byteOffsetEndLine.removePrefix("!").trim().toInt()
                        else
                            throw SynctexParseException(
                                "Invalid file format: The synctex top level node has " +
                                        "ended without byte offset record"
                            )
                        val elements = findBoxesInString(
                            contentLines.subList(lineCounter + 2, closingAt)
                                .joinToString("\n")
                        )
                        currentInputList.add(
                            if (nextit.startsWith("<")) {
                                SynctexForm(tag, elements, byteOffsetStart..byteOffsetEnd)
                            } else {
                                SynctexSheet(tag, elements, byteOffsetStart..byteOffsetEnd)
                            }
                        )
                        lineCounter = closingAt + 1
                        continue@loop
                    } // ignore else branch on purpose
                it.startsWith("Input:") -> {
                    if (currentInputList.isNotEmpty()) {
                        contentMap[currentInput] = currentInputList.toList()
                        currentInputList.clear()
                    }
                    val (_, tag, file) = it.split(":").map { it.trim() }
                    logger.debug { "Switching synctex input to $file" }
                    currentInput = SynctexInput(tag.toInt(), file)
                }
                else -> // this should not happen
                    logger.warn { "A strange line occurred (splitting): $it ($lineCounter)" }
            }
            lineCounter++
        }
        if (currentInputList.isNotEmpty())
            contentMap[currentInput] = currentInputList
        return SynctexContent(sheets = contentMap)
    }

    /**
     * This function parses the string representation of the Postscriptum
     * section to a [SynctexContent] object.
     *
     * @since 0.1.1
     * @param postscriptum The string representation of the postscriptum.
     * @return The parsed postscriptum.
     */
    private fun getSyncTeXPostscriptum(postscriptum: String):
            SynctexPostScriptum {
        // TODO: check with manpage (it says dimension, does that mean something
        // other than TeX scaled point?)
        return if (postscriptum.isNotEmpty()) {
            val postscriptMap = readToMap(postscriptum)
            SynctexPostScriptum(
                magnification = postscriptMap.getOrElse("Magnification",
                    { SynctexPostScriptum.DEFAULT_VALUE.toString() }).toInt(),
                xoffset = postscriptMap.getOrElse("X Offset",
                    { SynctexPostScriptum.DEFAULT_VALUE.toString() }).toInt(),
                yoffset = postscriptMap.getOrElse("Y Offset",
                    { SynctexPostScriptum.DEFAULT_VALUE.toString() }).toInt()
            )
        } else {
            SynctexPostScriptum()
        }
    }

    /**
     * This is the core parser function which parses a long string which follows
     * the synctex format into a object of type [SynctexFile].
     *
     * @param synctexContent The synctex compliant text. It is important that line
     *  ends are preserved.
     *
     * @return This returns an instance of [SynctexFile] with all text either
     *  compiled into objects or discarded.
     *
     * @throws SynctexParseException This method throws a syntax exception if the
     *  file is n synctex compliant file.
     * @throws NumberFormatException This method converts strings to integers as
     *  the synctex format requires it. If there is an invalid number
     *  representation the method will throw a [NumberFormatException].
     *
     * @since 0.1.0
     */
    @JvmStatic
    @Throws(SynctexParseException::class, NumberFormatException::class)
    public fun parse(synctexContent: String): SynctexFile {
        /**
         * This function is basically a [getOrDefault] version defaulting to a
         * exception.
         *
         * @since 0.1.0
         * @param key The key to look for.
         * @return The value from the map or an exception (but that is thrown).
         * @throws SynctexParseException If the map does not contain the given key
         *  which is required in the format a parse exception is thrown.
         */
        @Throws(SynctexParseException::class)
        fun Map<String, String>.getOrThrow(key: String): String {
            return this[key] ?: throw SynctexParseException(
                "Invalid file format: $key missing"
            )
        }

        // TODO: check with manpage: in reality there is not BOR (![0-9]+) before
        val contentRegex = "Content:".toRegex()
        // TODO: check with reality (manpage does not require postamble:)
        val postambleRegex = "![0-9]+\n(Postamble:\n)?Count:".toRegex()

        if (!synctexContent.contains(contentRegex) ||
            !synctexContent.contains(postambleRegex)
        )
            throw SynctexParseException(
                "Invalid file format: Content or Count " +
                        "label missing but required"
            )

        logger.debug { "Splitting synctex file for analysis" }
        // TODO: are the last byte offset records really important?
        val (preambleString, restString) = synctexContent
            .replace("\r\n", "\n").replace("\r", "\n")
            .split(contentRegex, limit = 2)
        val contentString = if (restString.contains("Postamble:"))
            restString.substringBefore("Postamble:")
        else
            restString.substringBefore("Count:")
        val postscriptumString = restString.substringAfterLast("Post scriptum:", "")
        val postambleString = restString.replace(contentString, "")
            .replace(postscriptumString, "")

        logger.debug { "Reading synctex preamble" }
        val preambleMap = readToMap(preambleString)
        val preamble = SynctexPreamble(
            synctexVersion = preambleMap.getOrThrow("SyncTeX Version").toFloat(),
            inputLines = preambleMap.getOrThrow("Input").split("\n").map {
                val (split1, split2) = it.split(":", limit = 2)
                SynctexInput(split1.toInt(), split2)
            },
            output = preambleMap.getOrDefault("Output", ""),
            magnification = preambleMap.getOrThrow("Magnification").toInt(),
            unit = preambleMap.getOrThrow("Unit").toInt(),
            xoffset = preambleMap.getOrThrow("X Offset").toInt(),
            yoffset = preambleMap.getOrThrow("Y Offset").toInt()
        )

        logger.debug { "Reading synctex content" }
        val content = getSyncTeXContent(contentString)

        logger.debug { "Reading synctex postamble" }
        val postamble = SynctexPostamble(
            count = readToMap(postambleString).getOrThrow("Count").toInt()
        )

        logger.debug { "Reading synctex postscriptum" }
        val postscriptum = getSyncTeXPostscriptum(postscriptumString)

        return SynctexFile(preamble, content, postamble, postscriptum)
    }
}
