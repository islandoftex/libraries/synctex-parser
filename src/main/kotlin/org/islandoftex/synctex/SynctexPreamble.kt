// SPDX-License-Identifier: BSD-3-Clause
package org.islandoftex.synctex

/**
 * This class resembles the synctex preamble which opens the file. It will give
 * some clues about the used files as well as the software.
 *
 * @since 0.1.0
 *
 * @property synctexVersion The version of the synctex software used to produce
 *  this synctex file.
 *  @since 0.1.0
 *
 * @property inputLines This list contains all lines used as synctex inputs.
 *  It includes the TeX files, aux files and possibly relevant packages, classes
 *  etc.
 *  @since 0.1.0
 *
 * @property output This value holds strings like "pdf", but is not part of the
 *  syntax specified on the manpage, so it's guesswork what this is intended
 *  for.
 *  @since 0.1.0
 *
 * @property magnification This value holds the TeX magnification which has been
 *  applied.
 *  @since 0.1.0
 *
 * @property unit This value holds the size of the unit in TeX scaled point.
 *  @since 0.1.0
 *
 * @property xoffset This value holds the x offset as scaled points.
 *  @since 0.1.0
 *
 * @property yoffset This value holds the x offset as scaled points.
 *  @since 0.1.0
 */
public data class SynctexPreamble(
    val synctexVersion: Number,
    val inputLines: List<SynctexInput>,
    val output: String,
    val magnification: Int,
    val unit: Int,
    val xoffset: Int,
    val yoffset: Int
)
