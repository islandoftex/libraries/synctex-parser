// SPDX-License-Identifier: BSD-3-Clause
package org.islandoftex.synctex

/**
 * This class resembles the real content of the synctex file. It follows the
 * structure of the file.
 *
 * @since 0.1.0
 *
 * @property sheets This property maps the current context ("input line") to the
 *  sheets or forms synctex associates with it. Please note that you'll only
 *  find [SynctexSheet] or [SynctexForm] elements inside that
 *  [SynctexTopLevelNode].
 *  @since 0.1.0
 */
public data class SynctexContent(val sheets: Map<SynctexInput, List<SynctexTopLevelNode>>) :
    Map<SynctexInput, List<SynctexTopLevelNode>> by sheets
