# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]

## [0.2.0] - 2020-08-20

### Added

* Abstraction layer for records.

### Changed

* Removed dependency on `kotlin-stdlib-jdk8`
* Made `SynctexStringUtils` internal (not in public API anymore,
  breaking change)
* Added `JvmStatic` annotation to `SynctexParser.parse` for easier use in
  Java code.
* Updated Kotlin to 1.4.0 and used explicit API mode.

### Fixed

* Reduced number of platform-dependent code (Kotlin collections).

## [0.1.0] - 2019-11-30

### Added

* Initial release.

[Unreleased]: https://gitlab.com/islandoftex/libraries/synctex-parser/compare/v0.2.0...master
[0.2.0]: https://gitlab.com/islandoftex/libraries/synctex-parser/compare/v0.1.0...v0.2.0
[0.1.0]: https://gitlab.com/islandoftex/libraries/synctex-parser/-/tags/v0.1.0
